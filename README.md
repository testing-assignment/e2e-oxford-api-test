# E2E test for Oxford Api 

This is build using java maven and api automation framework karate.


## Getting Started

## Pre-rquisite 
- Java 8
- Maven 3.6.3

### 1. Clone the repo
### 2. Run below command in root directory:
`mvn clean verify`
### 4. The execution report should be available
        target/surefire-reports/karate-summary.html


## Build Pipeline

1. Go to the link https://gitlab.com/testing-assignment/e2e-oxford-api-test/-/pipelines
2. Login to gitlab with provided username/password.
3. click on Run Pipeline


## Issues
 The test for translate feature are failing as the api is giving 403 for the endpoint with a valid token.
  ref: https://gitlab.com/testing-assignment/e2e-oxford-api-test/-/pipelines/283064550/test_report

  
