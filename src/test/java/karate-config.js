function fn() {    
  var env = karate.env; // get system property 'karate.env'
  karate.log('karate.env system property was:', env);
  if (!env) {
    env = 'dev';
  }
  var config = {
    env: env,
    url: 'https://od-api.oxforddictionaries.com/api/v2/',
    //key: karate.properties['key']
    key: '10fb191ccc81065e8f1825bed569168b',
    id: '9f67a690'
  }
  if (env == 'dev') {
    // customize
    // e.g. config.foo = 'bar';
  } else if (env == 'e2e') {
    // customize
  }


  return config;
}