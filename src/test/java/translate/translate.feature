Feature: get current weather
  Background:
    * url url

  Scenario Outline: get the origin of the word
    Given path 'translations/en/fr'
    And path <word>
    And params {fields: <fields>, strictMatch:false, lexicalCategory: <lexicalCategory>}
    And headers { Accept: 'application/json', app_id: '#(id)', app_key: '#(key)'}
    When method get
    Then status <response>

    Examples:
      | word   | fields       | response | lexicalCategory |
      | 'Test' | translations | 200      | noun            |
      | 'Test' | translations | 400      | Verb            |
      | 'Test' | translations | 404      | verb            |




